﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace QuieresSerMiNovia
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        int valida = 0;
        int contador = 0;
        

        private void BtnSi_Click(object sender, EventArgs e)
        {
            
            MessageBox.Show("Sabia que ibas a decir que sí", "Te Quiero", MessageBoxButtons.OK);
            valida = 1;
        }

        private void BtnCerrar_Click(object sender, EventArgs e)
        {
            if (valida == 1){
                Application.Exit();
            }
            else
            {
                MessageBox.Show("Debes seleccionar una respuesta...", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void BtnNo_Click(object sender, EventArgs e)
        {
           
        }

        private void BtnNo_MouseEnter(object sender, EventArgs e)
        {
            if (contador == 0)
            {
                btnNo.Left = 400;
                btnNo.Top = 300;
                contador++;
            }
            if (contador == 2)
            {
                btnNo.Left = 500;
                btnNo.Top = 90;
                contador++;

            }
            if (contador == 4)
            {
                btnNo.Left = 300;
                btnNo.Top = 250;
                contador++;

            }
            if (contador == 6)
            {
                btnNo.Left = 115;
                btnNo.Top = 20;
                contador ++;

            }
        }

        private void Form1_MouseLeave(object sender, EventArgs e)
        {
           
            if (contador == 1)
            {
                btnNo.Left = 50;
                btnNo.Top = 230;
                contador++;
            }
            if (contador == 3)
            {
                btnNo.Left = 430;
                btnNo.Top = 290;
                contador++;
                
            }
            if (contador == 5)
            {
                btnNo.Left = 44;
                btnNo.Top = 10;
                contador++;

            }
            if (contador == 7)
            {
                btnNo.Left = 320;
                btnNo.Top = 270;
                contador = 0;

            }
        }

        private void BtnNo_MouseMove(object sender, MouseEventArgs e)
        {

        }
    }
}
